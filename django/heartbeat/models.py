from django.db import models

class Heartbeat(models.Model):
    BEAT_TYPE = (
                 ('M','meditate'),
                 ('U','normal use'))
          
    session     = models.ForeignKey('Session')
    seconds     = models.BigIntegerField()
    latitude    = models.DecimalField(max_digits=20, decimal_places=10)
    longitude   = models.DecimalField(max_digits=20, decimal_places=10)
    beat_type   = models.CharField(max_length=1, default='U', choices=BEAT_TYPE)
    
    def as_dict(self):
        return {
#                 'id'    : self.id,
                'lat'   : str(self.latitude),
                'long'  : str(self.longitude),
#                 'sec'   : self.seconds,
                'type'  : self.beat_type
            }

class Session(models.Model):
    session     = models.CharField(max_length=64,unique=True)
    device      = models.CharField(max_length=64)
    ip          = models.GenericIPAddressField()
    
    def as_dict(self):
        return {
                'id'        : self.id,
                'session'   : str(self.session)
            }
        
    def get_id(self):
        return self.id