import time
import calendar
import json
import urllib2
import json     
from django.http import HttpResponse

from vgompa.siteconfig import Siteconfig

## -----------------------------------------------------
def query_to_json(recs, callback):
    dict = [ obj.as_dict() for obj in recs ]
    data = json.dumps({'data': dict})
    return HttpResponse(data, content_type='application/json')
                
## -----------------------------------------------------
def dict_to_json(dict):
    data = json.dumps(dict)
    return HttpResponse(data, content_type='application/json')

## -----------------------------------------------------
def query_to_json_short(recs):
    dict = [ obj.as_dict() for obj in recs ]
    return HttpResponse(dict, content_type='application/json')

## -----------------------------------------------------
def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

## -----------------------------------------------------
def get_time_since_1970():
    return calendar.timegm(time.gmtime())

## -----------------------------------------------------
def get_data_from_sec(sec):
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(sec))

## -----------------------------------------------------
def get_geo_info(ip):
    url = Siteconfig.geoip['url'] + "/json/";
    url += str(ip);
   
    try:
        data = json.load(urllib2.urlopen(url))
    except:
        print "Geo connection failed \"" + url + "\""
        return None

    msg = "Geo location for " + str(ip) + " is "
    msg += "lat: " + str(data[u"latitude"]) 
    msg += " long: " + str(data[u"longitude"]) 
    print msg
    return data