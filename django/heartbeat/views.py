import urllib2
import json
import random
import sys

from django.shortcuts import render
from django.http import HttpResponse

from tools import get_client_ip
from tools import get_time_since_1970
from tools import get_geo_info
from tools import query_to_json, query_to_json_short, dict_to_json

from vgompa.siteconfig import Siteconfig
from models import Heartbeat
from models import Session

# Create your views here.
def index(request):
    return HttpResponse("")

def get_rand_ip():
    ip1 = str(int(random.random()*240)+1)
    ip2 = str(int(random.random()*255))
    ip3 = str(int(random.random()*255))
    ip4 = str(int(random.random()*255))

    ret = ip1 + "." + ip2 + "." + ip3 + "." +ip4
    return ret


def beat_with_geo_base(request, session, mode, lat, long, sec):
    long = str(long);
    lat  = str(lat);
  
    try:
        h = Heartbeat.objects.create(session_id=session, seconds=sec, longitude=long, latitude=lat, beat_type=mode)
#         print h 
    except :
        HttpResponse("problem")
        pass
     
    return HttpResponse("")
    
def beat_with_geo(request, session, mode, lat, long):
    secs    = get_time_since_1970() 
    return beat_with_geo_base(request,session,mode,lat,long,secs)

def beat_with_random_ip(request):
    ip      = get_rand_ip()
    geoInfo = get_geo_info(ip)
    
#     u = 100000.0
#     long = int(random.gauss(116467615, u))
#     lat = int(random.gauss(39923488, u))
#     print "longitude=%d,latitude=%d" % (long, lat)
    if geoInfo == None :
        return HttpResponse("")
    
    long = geoInfo[u"longitude"];
    lat  = geoInfo[u"latitude"];

    if ( long == 0 and  lat == 0 ):
        return HttpResponse("")
 
    r = int(random.random() * 2)
    if r == 0 :
        r = int(random.random() * 2)
        
    if r == 0 :
        mode = 'U'
    else:
        mode = 'M'
        
    ses = int(random.random() * 1000000) * -1
    sec = get_time_since_1970() - int(random.random() * 30 * 24 * 60 * 60)
    
    return beat_with_geo_base(request,ses,mode,lat,long,sec)

def beat_with_ip(request, session, mode):
    ip      = get_client_ip(request)
#     ip      = get_rand_ip()
#     ip = "81.190.40.10"
#     ip = "217.98.224.10"
#     ip = "83.25.25.10"
#     ip = "217.98.224.10"
#     
    geoInfo = get_geo_info(ip)
    
    long = geoInfo[u"longitude"];
    lat  = geoInfo[u"latitude"];

    if (geoInfo == None ) or ( long == 0 and  lat == 0 ):
#     if geoInfo == None:
        return HttpResponse("")
 
    return beat_with_geo(request,session,mode,lat,long)


def beat_seed(request,seed,dev):
    ip      = get_client_ip(request)

    try:
        h = Session.objects.create(ip=ip, device=dev, session=seed)
    except :
# #         print "Unexpected error:", sys.exc_info()[0]
        pass

    ret = Session.objects.filter(session=seed)[:1]
    obj = ret[0]
    
    return HttpResponse( json.dumps( obj.get_id() ))

## -------------------------------------------------------
## Meditators
## -------------------------------------------------------
def meditators(request, seconds):
    sec = get_time_since_1970() - int(seconds)

    query = """
        SELECT * FROM 
                    (SELECT id,latitude,longitude,beat_type,session_ID
                            FROM heartbeat_heartbeat 
                                WHERE seconds > """ + str(sec) + """ 
                                ORDER BY id DESC 
                                 ) as Main 
                GROUP BY session_ID  """
    
    dict = [obj.as_dict() for obj in Heartbeat.objects.raw(query) ]
    return HttpResponse( json.dumps( dict ))

def meditators_online(request):
    return meditators(request,60)

def meditators_all(request):
    query = """
        SELECT * FROM 
                    (SELECT id,latitude,longitude,beat_type,session_ID
                            FROM heartbeat_heartbeat 
                            ORDER BY id DESC 
                            LIMIT 10000
                    ) as Main 
                GROUP BY session_ID
                LIMIT 3000  """
                

    dict = [obj.as_dict() for obj in Heartbeat.objects.raw(query) ]
    return HttpResponse( json.dumps( dict ))


def meditators_all_2(request):
    query = """
    SELECT * FROM (
        SELECT id, latitude, longitude, GROUP_CONCAT(latitude+'-'+longitude SEPARATOR '; ') as geo
        FROM heartbeat_heartbeat
        GROUP BY id) as main 
    GROUP BY geo;
                  """
                  
    dict = []
    for obj in Heartbeat.objects.raw(query) :
        t = { 'lat'   : str(obj.latitude),
         'long'  : str(obj.longitude),
         'type'  : obj.beat_type
        }

        dict.append(t)
        
    return HttpResponse( json.dumps( dict ))