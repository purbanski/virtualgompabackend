from django.conf.urls import patterns, url

from heartbeat import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
#     url(r'^beat$', views.beat, name='beat'),
#     url(r'^beat/meditate/dev/([0-9A-Za-z]{1,42})$', views.beat_meditate, name='beat_meditate'),
#     url(r'^beat/meditate/dev/([0-9A-Za-z]{1,42})/lat/([0-9]+\.[0-9]{0,})/long/([0-9]+\.[0-9]{0,})$', views.beat_meditate_with_geo, name='beat_meditate_with_geo'),
#     url(r'^beat/use/dev/([0-9A-Za-z]{1,42})$', views.beat_use, name='beat_use'),
#     url(r'^beat/use/dev/([0-9A-Za-z]{1,42})/lat/([0-9]+\.[0-9]{0,})/long/([0-9]+\.[0-9]{0,})$', views.beat_use_with_geo, name='beat_use_with_geo'),

    url(r'^beat/seed/([0-9A-Za-z]{1,42})/dev/([0-9A-Za-z]{1,42})$', views.beat_seed, name='beat_seed'),

    url(r'^beat/ses/([0-9]+)/mode/([MU])/lat/([0-9]+\.[0-9]{0,})/long/([0-9]+\.[0-9]{0,})$', views.beat_with_geo, name='beat_with_geo'),
    url(r'^beat/ses/([0-9]+)/mode/([MU])$', views.beat_with_ip, name='beat_with_ip'),
    url(r'^beat/random$', views.beat_with_random_ip, name='beat_with_random_ip'),

#     url(r'^meditators$', views.meditators, name='meditators'),
#     url(r'^meditators_all$', views.meditators_all, name='meditators_all'),

    url(r'^meditators/([0-9]{1,7})$', views.meditators, name='meditators'),
    url(r'^meditators/online$', views.meditators_online, name='meditators_online'),
    url(r'^meditators/all$', views.meditators_all, name='meditators_all'),
)
