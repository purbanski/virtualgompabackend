class Siteconfig :
    db = {'name' :      'vgompa',
          'user' :      'root',
          'password' :  'dupadupa',
          'host' :      'localhost' }
    
    geoip = { 'url' : 'http://192.168.3.2:4477' }
    
    debug = True
    
    heartbeat = { 'query-last-sec' : 60 }
    allowed_hosts = [ 'vgdev.blackted.com' ]
