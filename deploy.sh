#!/bin/sh
DIR=django/vgompa

rm -rf $DIR/siteconfig.py
mv $DIR/siteconfig-deploy.py $DIR/siteconfig.py

chmod 440 $DIR/siteconfig.py
chown root.apache $DIR/siteconfig.py

GEOIP_DIR=freegeoip
pushd $GEOIP_DIR &> /dev/null
for d in `find -type d`; do
	chmod o-rwx $d
	chown vgompa.root $d
done

for f in `find -type f`; do 
	chmod o-rwx $f
	chown vgompa.root $f 
done

popd &> /dev/null

rm -rf deploy.sh