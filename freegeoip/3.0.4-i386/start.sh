#!/bin/sh

EXEC=freegeoip
OPTS='--addr=127.0.0.1:4477'
EXECDIR='/var/www/virtuals/vgompa-backend/freegeoip/3.0.4-i386'

ln=`ps aux|grep \[f\]reegeoip|wc -l`

if [ -z "$ln" ]; then 
exit 0
fi

if [ $ln -eq 3 ]; then
	echo Already running
	
else if [ $ln -eq 2 ]; then
	echo Starting
	pushd $EXECDIR &> /dev/null
	exec ./$EXEC $OPTS  &> /dev/null &
	popd &> /dev/null
fi

fi 